﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop
{
    public class DataBase
    {
        private User _users;

        public DataBase()
        {
            this._users = null;
        }

        public User GetUsers()
        {
            return this._users;
        }

        public void SetNewUser(User u)
        { 
            if (this._users == null)
            {
                this._users = u;
            }
            else
            {
                User curr = this._users;
                while (curr.GetNext() != null)
                {
                    curr = curr.GetNext();
                }
                curr.SetNext(u);
            }      
        }

        public bool IfUsernameExists(string username)
        {
            bool ret = false;
            User curr = this._users;
            while (curr != null)
            {
                if (curr.GetUsername().Equals(username))
                {
                    ret = true;
                }
                curr = curr.GetNext();
            }
            return ret;
        }

        public bool IfUserExists(string username, string password)
        {
            bool ret = false;
            User curr = this._users;
            while (curr != null)
            {
                if (curr.GetUsername().Equals(username) && curr.GetPassword().Equals(password))
                {
                    ret = true;
                }
                curr = curr.GetNext();
            }
            return ret;
        }
    }
}
