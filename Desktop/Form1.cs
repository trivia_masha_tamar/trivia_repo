﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop
{
    public partial class Form1 : Form
    {
        DataBase _db;

        public Form1(DataBase db)
        {
            _db = db;
            InitializeComponent();
        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            SignIn s = new SignIn(_db);
            this.Hide();
            s.Show();
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            LogIn l = new LogIn(_db);
            this.Hide();
            l.Show();
        }
    }
}
