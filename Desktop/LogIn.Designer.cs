﻿namespace Desktop
{
    partial class LogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUserLog = new System.Windows.Forms.Label();
            this.txtPassLog = new System.Windows.Forms.Label();
            this.txtBxUserLog = new System.Windows.Forms.TextBox();
            this.txtBxPassLog = new System.Windows.Forms.TextBox();
            this.txtLogIn = new System.Windows.Forms.Label();
            this.btnLog = new System.Windows.Forms.Button();
            this.msg = new System.Windows.Forms.Label();
            this.btnSign = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtUserLog
            // 
            this.txtUserLog.AutoSize = true;
            this.txtUserLog.Location = new System.Drawing.Point(382, 234);
            this.txtUserLog.Name = "txtUserLog";
            this.txtUserLog.Size = new System.Drawing.Size(58, 13);
            this.txtUserLog.TabIndex = 0;
            this.txtUserLog.Text = "Username:";
            // 
            // txtPassLog
            // 
            this.txtPassLog.AutoSize = true;
            this.txtPassLog.Location = new System.Drawing.Point(384, 263);
            this.txtPassLog.Name = "txtPassLog";
            this.txtPassLog.Size = new System.Drawing.Size(56, 13);
            this.txtPassLog.TabIndex = 1;
            this.txtPassLog.Text = "Password:";
            // 
            // txtBxUserLog
            // 
            this.txtBxUserLog.Location = new System.Drawing.Point(493, 227);
            this.txtBxUserLog.Name = "txtBxUserLog";
            this.txtBxUserLog.Size = new System.Drawing.Size(100, 20);
            this.txtBxUserLog.TabIndex = 2;
            // 
            // txtBxPassLog
            // 
            this.txtBxPassLog.Location = new System.Drawing.Point(493, 256);
            this.txtBxPassLog.Name = "txtBxPassLog";
            this.txtBxPassLog.Size = new System.Drawing.Size(100, 20);
            this.txtBxPassLog.TabIndex = 3;
            // 
            // txtLogIn
            // 
            this.txtLogIn.AutoSize = true;
            this.txtLogIn.Location = new System.Drawing.Point(468, 184);
            this.txtLogIn.Name = "txtLogIn";
            this.txtLogIn.Size = new System.Drawing.Size(36, 13);
            this.txtLogIn.TabIndex = 4;
            this.txtLogIn.Text = "Log in";
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(456, 309);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(75, 23);
            this.btnLog.TabIndex = 5;
            this.btnLog.Text = "Log in";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // msg
            // 
            this.msg.AutoSize = true;
            this.msg.Location = new System.Drawing.Point(468, 362);
            this.msg.Name = "msg";
            this.msg.Size = new System.Drawing.Size(0, 13);
            this.msg.TabIndex = 6;
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(456, 403);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(75, 23);
            this.btnSign.TabIndex = 7;
            this.btnSign.Text = "Sign up!";
            this.btnSign.UseVisualStyleBackColor = true;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // LogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(950, 494);
            this.Controls.Add(this.btnSign);
            this.Controls.Add(this.msg);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.txtLogIn);
            this.Controls.Add(this.txtBxPassLog);
            this.Controls.Add(this.txtBxUserLog);
            this.Controls.Add(this.txtPassLog);
            this.Controls.Add(this.txtUserLog);
            this.Name = "LogIn";
            this.Text = "LogIn";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtUserLog;
        private System.Windows.Forms.Label txtPassLog;
        private System.Windows.Forms.TextBox txtBxUserLog;
        private System.Windows.Forms.TextBox txtBxPassLog;
        private System.Windows.Forms.Label txtLogIn;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Label msg;
        private System.Windows.Forms.Button btnSign;
    }
}