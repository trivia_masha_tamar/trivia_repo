﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop
{
    public partial class LogIn : Form
    {
        DataBase _db;
        public LogIn(DataBase db)
        {
            
            _db = db;
            InitializeComponent();
            btnSign.Hide();
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            if (_db.IfUserExists(txtBxUserLog.Text, txtBxPassLog.Text))
            {
                Options o = new Options();
                this.Hide();
                o.Show();
            }
            else
            {
                msg.Text = "The user does not exists. You can sign up.";
                btnSign.Show();
            }
        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            SignIn s = new SignIn(_db);
            this.Hide();
            s.Show();
        }
    }
}
