﻿namespace Desktop
{
    partial class SignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUser = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.Label();
            this.txtConfirm = new System.Windows.Forms.Label();
            this.txtSign = new System.Windows.Forms.Label();
            this.txtBxUser = new System.Windows.Forms.TextBox();
            this.txtBxPass = new System.Windows.Forms.TextBox();
            this.txtBxConfirm = new System.Windows.Forms.TextBox();
            this.btnSign = new System.Windows.Forms.Button();
            this.msg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtUser
            // 
            this.txtUser.AutoSize = true;
            this.txtUser.Location = new System.Drawing.Point(338, 237);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(58, 13);
            this.txtUser.TabIndex = 0;
            this.txtUser.Text = "Username:";
            this.txtUser.Click += new System.EventHandler(this.txtUser_Click);
            // 
            // txtPass
            // 
            this.txtPass.AutoSize = true;
            this.txtPass.Location = new System.Drawing.Point(338, 267);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(56, 13);
            this.txtPass.TabIndex = 1;
            this.txtPass.Text = "Password:";
            // 
            // txtConfirm
            // 
            this.txtConfirm.AutoSize = true;
            this.txtConfirm.Location = new System.Drawing.Point(338, 297);
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.Size = new System.Drawing.Size(93, 13);
            this.txtConfirm.TabIndex = 2;
            this.txtConfirm.Text = "Confirm password:";
            this.txtConfirm.Click += new System.EventHandler(this.txtConfirm_Click);
            // 
            // txtSign
            // 
            this.txtSign.AutoSize = true;
            this.txtSign.Location = new System.Drawing.Point(453, 198);
            this.txtSign.Name = "txtSign";
            this.txtSign.Size = new System.Drawing.Size(43, 13);
            this.txtSign.TabIndex = 3;
            this.txtSign.Text = "Sign up";
            // 
            // txtBxUser
            // 
            this.txtBxUser.Location = new System.Drawing.Point(503, 230);
            this.txtBxUser.Name = "txtBxUser";
            this.txtBxUser.Size = new System.Drawing.Size(100, 20);
            this.txtBxUser.TabIndex = 4;
            // 
            // txtBxPass
            // 
            this.txtBxPass.Location = new System.Drawing.Point(503, 260);
            this.txtBxPass.Name = "txtBxPass";
            this.txtBxPass.Size = new System.Drawing.Size(100, 20);
            this.txtBxPass.TabIndex = 5;
            // 
            // txtBxConfirm
            // 
            this.txtBxConfirm.Location = new System.Drawing.Point(503, 290);
            this.txtBxConfirm.Name = "txtBxConfirm";
            this.txtBxConfirm.Size = new System.Drawing.Size(100, 20);
            this.txtBxConfirm.TabIndex = 6;
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(438, 340);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(75, 23);
            this.btnSign.TabIndex = 7;
            this.btnSign.Text = "Sign up";
            this.btnSign.UseVisualStyleBackColor = true;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // msg
            // 
            this.msg.AutoSize = true;
            this.msg.Location = new System.Drawing.Point(453, 387);
            this.msg.Name = "msg";
            this.msg.Size = new System.Drawing.Size(0, 13);
            this.msg.TabIndex = 8;
            // 
            // SignIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(947, 496);
            this.Controls.Add(this.msg);
            this.Controls.Add(this.btnSign);
            this.Controls.Add(this.txtBxConfirm);
            this.Controls.Add(this.txtBxPass);
            this.Controls.Add(this.txtBxUser);
            this.Controls.Add(this.txtSign);
            this.Controls.Add(this.txtConfirm);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.txtUser);
            this.Name = "SignIn";
            this.Text = "SignIn";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtUser;
        private System.Windows.Forms.Label txtPass;
        private System.Windows.Forms.Label txtConfirm;
        private System.Windows.Forms.Label txtSign;
        private System.Windows.Forms.TextBox txtBxUser;
        private System.Windows.Forms.TextBox txtBxPass;
        private System.Windows.Forms.TextBox txtBxConfirm;
        private System.Windows.Forms.Button btnSign;
        private System.Windows.Forms.Label msg;
    }
}