﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop
{
    public partial class SignIn : Form
    {
        const int _size = 5;
        DataBase _db;
        public SignIn(DataBase db)
        {
            _db = db;
            InitializeComponent();
        }

        private void txtUser_Click(object sender, EventArgs e)
        {

        }

        private void txtConfirm_Click(object sender, EventArgs e)
        {

        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            if (!_db.IfUsernameExists(txtBxUser.Text))
            {
                if (txtBxUser.TextLength >= _size)
                {
                    if (txtBxPass.Text.Equals(txtBxConfirm.Text))
                    {
                        if (txtBxPass.TextLength >= _size)
                        {
                            User u = new User(txtBxUser.Text, txtBxPass.Text);
                            _db.SetNewUser(u);
                            Form1 f = new Form1(_db);
                            this.Hide();
                            f.Show();
                        }
                        else
                        {
                            msg.Text = "The password must contain 5 characters at least.";
                        }
                    }
                    else
                    {
                        msg.Text = "Passwords aren't equal.";
                    }
                }
                else
                {
                    msg.Text = "The username must contain 5 characters at least.";
                }
            }
            else
            {
                msg.Text = "The user already exists.";
            }
            
        }
    }
}
