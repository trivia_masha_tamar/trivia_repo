﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop
{
    public class User
    {
        private string _username;
        private string _password;
        private int _highScore;
        User _next;

        public User(string us, string pass)
        {
            this._username = us;
            this._password = pass;
            this._highScore = 0;
        }

        public string GetUsername()
        {
            return this._username;
        }

        public string GetPassword()
        {
            return this._password;
        }

        public void SetHighScore(int s)
        {
            this._highScore = s;
        }

        public int GetHighScore()
        {
            return this._highScore;
        }

        public void SetNext(User next)
        {
            this._next = next;
        }

        public User GetNext()
        {
            return this._next;
        }
    }
}
